# Wardrobify

Team:

* Terence - Hats microservice

* Elaine - Shoes microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

Wardrobify uses React on the front-end and contains a 2 Shoes microservices:
shoes/api
A Django Project with a Django App
The Shoes Django model is broken down into manufacturer, model, picture and color.
Wardrobe data can be found in the Shoes BinVO model class with the following fields: closet_name, bin_number, bin_size and import_href.
shoes/poll
A polling application that uses the Django resources in the RESTful API project
Data is polled from the Wardrobe microservice every 60 seconds.

Database information can be viewed on the React front end under /shoes with the ability to delete entries. Entries can be created at /shoes/create. The React server runs on http://localhost:3000. The Shoes API Microservice runs on http://localhost:8080. Wardrobe Microservice runs http://localhost:8100. The poller uses the base URL http://wardrobe-api:8000.

It has full RESTful APIs to interact with the Shoes Bin resources.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

hats/api is a Django backend with a Javascript/React frontend. The Django hat API application can be accessed on
port 8090. The Wardrobe API application can be accessed on port 8100.
In the Django application:
- Created models
- Created views functions to list the hats and hat details
- Created encoders for the hats
- Added paths to urls
- Created a poller to fetch data from Wardrobe API

In React/Javascript:
- Created a form for adding a new hat
- Created a page to list all hats in the collection
- Added a delete function to delete the hats if needed
