# from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import Hats, LocationVO
from common.json import ModelEncoder

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
    ]

class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "id",
    ]
    def get_extra_data(self, o):
        return {
            "fabric": o.fabric,
            "color": o.color,
            "picture": o.picture,
            "location": o.location.closet_name,
        }

class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "fabric",
        "color",
        "picture",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
            safe=False,
        )
    else: # POST METHOD
        content = json.loads(request.body)
        try:
            location_href = f"/api/locations/{location_vo_id}/"
            print(location_href, "location href")
            location = LocationVO.objects.get(import_href = location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Could not find the Location"},
                status = 400,
            )
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe = False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hats = Hats.objects.get(id=pk)
        return JsonResponse(
            {"hats": hats},
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        request.method == "DELETE"
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    # else: # PUT METHOD
    #     content = json.loads(request.body)
    #     try:
    #         if "location" in content:
    #             location = LocationVO.objects.get(id=["location"])
    #             content["location"] = location
    #     except LocationVO.DoesNotExist:
    #         return JsonResponse(
    #             {"message": "Could not find the shoe you are looking for"},
    #             status = 400,
    #         )
    #     Hats.objects.filter(id=pk).update(**content)
    #     hats = Hats.objects.get(id=pk)
    #     return JsonResponse(
    #         hats,
    #         encoder=HatDetailEncoder,
    #         safe=False
    #     )
