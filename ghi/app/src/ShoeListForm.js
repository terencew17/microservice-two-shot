import React, { useEffect, useState } from "react";

function ShoesListForm() {
    // if (props.shoes === undefined) {
    //     return null;
    // }

    const[shoes, setShoes] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/list/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setShoes(data.shoes)
        } else {
            console.log(response)
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    async function deleteShoe(shoe) {
        const deleteUrl = `http://localhost:8080/api/list/${shoe.id}/`
        const response = await fetch(deleteUrl, { method: "delete" })
        if (response.ok) {
            console.log(`You just deleted ${shoe.name}`)

        }
        window.location.reload();
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Picture URL</th>
                    <th>Closet</th>
                    <th>Bin #</th>
                    <th>Bin Size</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map((shoe, id) => {
                    return (
                        <tr key={id}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.picture}</td>
                            <td>{shoe.closet_name}</td>
                            <td>{shoe.bin_number}</td>
                            <td>{shoe.bin_size}</td>
                            <td><button onClick={() => deleteShoe(shoe)} type="button" className="btn btn-danger">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}






export default ShoesListForm;

{/* <button onClick={() => this.delete(hat.id)}></button> */ }
