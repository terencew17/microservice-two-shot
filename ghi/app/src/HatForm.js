import React, {useEffect, useState } from 'react';

function HatForm() {

    const[locations, setLocations] = useState([]);
    const[location, setLocation] = useState('')
    const[styleName, setStyleName] = useState('');
    const[fabric, setFabric] = useState('');
    const[color, setColor] = useState('');
    const[picture, setPicture] = useState('');

    const handleStyleNameChange = (event) => {
        setStyleName(event.target.value);
    }
    const handleFabricChange = (event) => {
        setFabric(event.target.value);
    }
    const handleColorChange = (event) => {
        setColor(event.target.value);
    }
    const handlePictureChange = (event) => {
        setPicture(event.target.value);
    }
    const handleLocationChange = (event) => {
        setLocation(event.target.value);
    }


    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
            console.log(locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.location = location;
        data.fabric = fabric;
        data.color = color;
        data.picture = picture;
        data.style_name = styleName;

        const hatUrl = `http://localhost:8090/api/locations/${data.location}/hats/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch (hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setStyleName('');
            setFabric('');
            setColor('');
            setPicture('');
            setLocation('');
        }

    }

function Redirect() {
    window.location.href = 'http://localhost:3000/hats'
}
    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Want to add a Hat?</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={handleStyleNameChange} value={styleName} placeholder="Style Name" required type="text" name="name" id="name" className="form-control" />
                    <label forhtml="name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFabricChange} value={fabric} placeholder="Fabric Type" required type="text" name="fabric" id="name" className="form-control" />
                    <label forhtml="starts">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="ends" id="name" className="form-control" />
                    <label forhtml="starts">Color</label>
                </div>
                <div className="mb-3">
                    <label forhtml="description">Picture URL</label>
                    <textarea onChange={handlePictureChange} value={picture} className="form-control" name="picture" id="picture"></textarea>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocationChange} value={location} required id="location" name="location" className="form-select">
                        <option value="">Choose a location</option>
                        {locations?.map(location => {
                        return (
                            <option key={location.href} value={location.id}>
                                {location.closet_name}
                            </option>
                        )
                        })}
                    </select>
                </div>
                <button onClick={() => Redirect()} className="btn btn-primary">Create</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    )
}
export default HatForm
