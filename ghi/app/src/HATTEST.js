import React, { useEffect, useState } from "react";

function HatList(props){
    if(props.hats === undefined){
        return null;
      }
    // const [hats, setHats] = useState([]);
    // const fetchData = async () => {
    //   const url = 'http://localhost:8090/api/hats/'
    //   const response = await fetch(url);

    //   if (response.ok) {
    //     const data = await response.json();
    //     console.log(data)
    //     setHats(data.hats)
    //   }
    // }

    // useEffect(() => {
    //   fetchData();
    // }, [])

      async function deleteHats(hat) {
        const deleteUrl = `http://localhost:8090/api/hats/${hat.id}/`
        const response = await fetch(deleteUrl, {method:"delete"})
        if (response.ok) {
          console.log("You just deleted this hat", response)
        }
        window.location.reload();
      }
    return (
    <div>
        <div className="justify-content-center">
        {props.hats.map((hat, href) => {
          return (
            <div key={ href } className="card w-50 mb-5 shadow">
              <img src={ hat.picture } className="card-img-top img-thumbnail-border" />
              <div className="card-body">
                <h4 className="card-title text-center">{ hat.style_name }</h4>
                <p className="card-subtitle mb-2 text-center">
                  { hat.fabric }
                </p>
                <p className="card-text text-center">
                  { hat.color }
                </p>
              </div>
              <div className="card-footer">
                { hat.location }
                <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button onClick={() => deleteHats(hat)} type="button" className="btn btn-danger btn-sm">Delete</button>
                </div>
              </div>
            </div>
          );
        })}
        </div>
    </div>
    );
  }
export default HatList;
