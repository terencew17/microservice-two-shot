import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState("");
    const [model, setModel] = useState("");
    const [color, setColor] = useState("");
    const [picture, setPicture] = useState("");
    const [bin, setBin] = useState("");

    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    }
    const handleModelChange = (event) => {
        setModel(event.target.value);
    }
    const handleColorChange = (event) => {
        setColor(event.target.value);
    }
    const handlePictureChange = (event) => {
        setPicture(event.target.value);
    }
    const handleBinChange = (event) => {
        setBin(event.target.value);
    }


    const fetchData = async () => {
        const binUrl = 'http://localhost:8100/api/bins/';

        const response = await fetch(binUrl);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
            console.log(bins);
            // const selectTag = document.getElementById('state');
            // for (let state of data.states) {
            //   const option = document.createElement('option');
            //   option.value = state.abbreviation;
            //   option.innerHTML = state.name;
            //   selectTag.appendChild(option);
        }
    }


    useEffect(() => { fetchData(); }, []);

    // const resetState = () => {
    //     setManufacturer("");
    //     setModel("");
    //     setPicture("");
    //     setColor("");

    // }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.manufacturer = manufacturer;
        data.model = model;
        data.picture = picture;
        data.color = color;
        data.bin = bin;

        console.log(data);
        // resetState();

        const url = `http://localhost:8080/api/list/`;

        //const url = "http://localhost:8080/api/bins/2/shoes";

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newShoes = await response.json();
            console.log(newShoes);

            setManufacturer('');
            setModel('');
            setPicture('');
            setColor('');
            setBin('');
        }

    }



    return (
        <div className='my-5 container'>
            {/* // <div className="row"> */}
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange}
                                required name="manufacturer"
                                placeholder="Manufacturer"
                                type="text"
                                id="manufacturer"
                                className="form-control"
                                value={manufacturer}
                            />
                            <label htmlFor="Manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleModelChange}
                                required name="model"
                                placeholder="Model"
                                type="text"
                                id="model"
                                className="form-control"
                                value={model}
                            />
                            <label htmlFor="model">Model</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureChange}
                                value={picture}
                                required name="picture"
                                placeholder="Picture"
                                type="url"
                                id="picture"
                                className="form-control"
                            />
                            <label htmlFor="picture">Picture URL</label>
                        </div>

                        <div className="form-floating mb-3">

                            <input onChange={handleColorChange} required name="color" placeholder="Color" type="text" id="color" className="form-control" value={color} />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                                <option value="">Choose a bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                        // comes from line 18 option.value = location.id; of new-conference.js
                                    );
                                })}
                            </select>

                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}



export default ShoeForm;
