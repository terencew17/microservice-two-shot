import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HATTEST';
import HatForm from './HatForm';
import ShoeForm from './ShoeForm';
import ShoeListForm from './ShoeListForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatList hats={props.hats}/>} />
            <Route path="/hats/create" element={<HatForm />} />
          </Route>
          <Route>
            <Route path="/shoes" element={<ShoeListForm shoes={props.shoes} />} />
            <Route path="/shoes/create" element={<ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
