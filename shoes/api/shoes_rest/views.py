from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Shoes, BinVO
import json
from django.http import JsonResponse
from common.json import ModelEncoder

# Create your views here.
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
    ]

class ShoesEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model",
        "color",
        "picture",
        "bin",
        "id",
    ]
    encoders = {
        "bin" : BinVOEncoder()
    }

class ShoesNameEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model",
        "manufacturer",
        "color",
        "picture",
        "id",
    ]
    def get_extra_data(self, o):
        return {
            "closet_name": o.bin.closet_name,
        }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesNameEncoder,
        )
    else:

        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            print(bin_href, "Print bin_href")
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Error in api list shoes"},
                status=400,
            )

        shoes= Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_shoe_details(request, pk):

    if request.method == "GET":
        try:
            shoes = Shoes.objects.get(id=pk)
            return JsonResponse(
                shoes,
                encoder=ShoesEncoder,
                safe=False
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"error": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoes = Shoes.objects.get(id=pk)
            shoes.delete()
            return JsonResponse(
                shoes,
                encoder=ShoesEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            return JsonResponse({"error": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            shoes = Shoes.objects.get(id=pk)

            props = ["manufacturer", "model", "color", "picture",]
            for prop in props:
                if prop in content:
                    setattr(shoes, prop, content[prop])
            shoes.save()
            return JsonResponse(
                shoes,
                encoder=ShoesEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"error": "Does not exist"})
            response.status_code = 404
            return response
