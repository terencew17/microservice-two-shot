from django.urls import path
from .views import api_list_shoes, api_shoe_details

urlpatterns = [
    path("list/", api_list_shoes, name="api_list_shoes"),
    path("list/<int:pk>/", api_shoe_details, name="api_shoe_details"),
    path("bins/<int:bin_vo_id>/shoes/", api_list_shoes, name ="api_shoe_bin")
]
